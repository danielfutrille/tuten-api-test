# Tuten API Test

## Pre requisitos y Versiones
- Version de Java: `11.0.10`
- Maven: `3.6.3`


## Pasos para instalación
- Clonar repositorio desde: [https://gitlab.com/danielfutrille/tuten-api-test.git](https://gitlab.com/danielfutrille/tuten-api-test.git)
- Desde el directorio raiz (tuten-api-test) ejecutar lo siguiente: `mvn clean install spring-boot:run`

El servicio se iniciará por defecto en `http://localhost:8080`


## Pruebas
- Endpoint de prueba: `POST /time?data1={data1}&data2={data2}`
> Se asume el tipo de dato para **data1** como String y para **data2** como Entero.

Donde:
- **data1**: representa una hora en formato 24 Horas. Ejemplo: `15:30:00`
- **data2**: representa la diferencia horaria como un numero entero entre -18 y 18.

**Ejemplo**: `/time?data1=18:00:00&data2=-4`. El JSON resultante deberia ser así:
```
{
    "response": {
        "time": "22:00:00",
        "timezone": "utc"
    }
}
```

## Cliente para Pruebas (Opcional)
- **Postman**: Descargar el siguiente [Collection](https://www.getpostman.com/collections/88b6ca9e8cbb4bb17b81) para ejecutar la prueba.


## Consideraciones
- Si el valor de **data1** es incorrecto, tendra un JSON resultante con el siguiente cuerpo:
```
{
    "timestamp": 1619566798173,
    "message": "Time format not allowed."
}
```
- Si el valor de **data2** es incorrecto, tendra un JSON resultante con el siguiente cuerpo:
```
{
    "timestamp": 1619566897188,
    "message": "Invalid TimeZone number."
}
```
- Al iniciar el servicio se ejecutaran 3 pruebas unitarias:
    - **shouldConvertTime**: prueba del calculo correcto de la hora.
    - **whenInvalidFormat**: validación al ingresar un formato invalido de hora.
    - **whenInvalidTimeZone**: validación al ingresar un timezona incorrect.


