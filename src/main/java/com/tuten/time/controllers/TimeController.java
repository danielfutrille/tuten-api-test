package com.tuten.time.controllers;

import java.util.Map;

import com.tuten.time.models.DTO.TimeDTO;
import com.tuten.time.services.TimeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class TimeController {
    @Autowired
    private TimeService timeService;

    @PostMapping("time")
    public Map<String, TimeDTO> getTimeUTC(@RequestParam(name = "data1") String data1,
            @RequestParam(name = "data2") Integer data2) {
        return timeService.getTimeUTC(data1, data2);
    }

}
