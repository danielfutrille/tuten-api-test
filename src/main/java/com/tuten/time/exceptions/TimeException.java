package com.tuten.time.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TimeException extends RuntimeException {
    
    private final String message;

    public TimeException(String errorMessage) {
        super(errorMessage);

        this.message = errorMessage;
    }

    public TimeException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);

        this.message = errorMessage;
    }
}
