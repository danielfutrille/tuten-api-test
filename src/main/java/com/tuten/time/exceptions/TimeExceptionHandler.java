package com.tuten.time.exceptions;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@EnableWebMvc
@ControllerAdvice
@RestController
@Component
public class TimeExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(TimeException.class)
    public final ResponseEntity<Object> handleTimeException(TimeException ex, WebRequest request) {
        TimeExceptionReponse exceptionResponse = new TimeExceptionReponse(new Date(), ex.getMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
    }
}
