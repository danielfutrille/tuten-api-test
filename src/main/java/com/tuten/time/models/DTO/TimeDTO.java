package com.tuten.time.models.DTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TimeDTO {
    
    private String time;
    private String timezone = "utc";

    public TimeDTO(String time, String timezone) {
        this.time = time;
        this.timezone = timezone;
    }
}
