package com.tuten.time.services;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import com.tuten.time.exceptions.TimeException;
import com.tuten.time.models.DTO.TimeDTO;

import org.springframework.stereotype.Service;

@Service
public class TimeService {

    public Map<String, TimeDTO> getTimeUTC(String stringTime, Integer intTimeZone) {
        HashMap<String, TimeDTO> response = new HashMap<>();

        TimeDTO timeResult = new TimeDTO();

        try {
            // Validations
            if (!stringTime.matches("^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])$")) {
                throw new TimeException("Time format not allowed.");
            }
            
            if (intTimeZone < -18 || intTimeZone > 18) {
                throw new TimeException("Invalid TimeZone number.");
            }

            // Formatters
            DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            DateTimeFormatter longFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

            // Get a valid DateTime
            String date = LocalDateTime.now().format(dateFormatter);
            LocalDateTime validDateTime = LocalDateTime.parse(date + " " + stringTime, longFormatter);

            // Apply User TimeZone
            ZoneOffset zoneOffSet = ZoneOffset.ofHours(intTimeZone);
            ZonedDateTime zonedDateTime = validDateTime.atZone(ZoneId.ofOffset("", zoneOffSet));

            // Get UTC Time
            ZonedDateTime utcDateTime = zonedDateTime.withZoneSameInstant(ZoneOffset.UTC);
            timeResult.setTime(utcDateTime.format(timeFormatter));

            response.put("response", timeResult);
        } catch (Exception e) {
            throw new TimeException(e.getMessage());
        }

        return response;
    }
}
