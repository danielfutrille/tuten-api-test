package com.tuten.time;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TimeApplicationTests {

	@Test
	void contextLoads() {
		Assertions.assertThatNoException();
	}
	
}
