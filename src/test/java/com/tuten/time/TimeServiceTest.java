package com.tuten.time;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;

import com.tuten.time.exceptions.TimeException;
import com.tuten.time.models.DTO.TimeDTO;
import com.tuten.time.services.TimeService;

import org.junit.jupiter.api.Test;

public class TimeServiceTest {

    @Test
    void shouldConvertTime() {
        TimeService timeService = new TimeService();
        Map<String, TimeDTO> response = timeService.getTimeUTC("18:00:00", -18);

        assertEquals("12:00:00", response.get("response").getTime());
    }
    
    @Test
    public void whenInvalidFormat() {
        TimeService timeService = new TimeService();
        Exception exception = assertThrows(TimeException.class, () -> {
            timeService.getTimeUTC("25:00:00", +2);
        });
    
        String expectedMessage = "Time format not allowed.";
        String actualMessage = exception.getMessage();
    
        assertTrue(actualMessage.contains(expectedMessage));
    }
    
    @Test
    public void whenInvalidTimeZone() {
        TimeService timeService = new TimeService();
        Exception exception = assertThrows(TimeException.class, () -> {
            timeService.getTimeUTC("18:00:00", 48);
        });
    
        String expectedMessage = "Invalid TimeZone number.";
        String actualMessage = exception.getMessage();
    
        assertTrue(actualMessage.contains(expectedMessage));
    }
    
}
